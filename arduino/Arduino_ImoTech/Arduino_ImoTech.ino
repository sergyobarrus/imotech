/*
Broker: https://shiftr.io/t
*/
#include<Narcoleptic.h>
#include <dht.h>
#include <SPI.h>
#include <Ethernet.h>
#include <MQTTClient.h>
#include <SoftwareSerial.h>

unsigned long currentTime;
unsigned long loopTime;

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
/*byte ip[] = { 192, 168, 0, 25 }; // <- change to match your network*/
char message_buff[100]; // this buffers our incoming messages so we can do something on certain commands

EthernetClient net;
MQTTClient client;

dht DHT;
#define DHT11_PIN 5

/* variaveis */
unsigned long readTime;
const int actionOn_Portao = 9;
const int actionOff_Portao = 8;

const int actionOn_Porta = 7;
const int actionOff_Porta = 6;

const int openJanela_Cozinha = 4;
const int openJanela_QuartoSuite = 3;
const int openJanela_Escritorio = 2;

void setup() {
  Serial.begin(115200);
  Serial.begin(9600);
  Ethernet.begin(mac);
  //Ethernet.begin(mac, ip);

  /* saída de sinais */
  pinMode(actionOn_Portao, OUTPUT);
  pinMode(actionOff_Portao, OUTPUT);
  
  pinMode(actionOn_Porta, OUTPUT);
  pinMode(actionOff_Porta, OUTPUT);

  pinMode(openJanela_Cozinha, OUTPUT);
  pinMode(openJanela_QuartoSuite, OUTPUT);
  pinMode(openJanela_Escritorio, OUTPUT);

  readTime = 0;
  /* shiftr.io */
  client.begin("broker.shiftr.io", net);

  connect();
}

void loop() {

  /* Verificar conexao com o broker*/
  if (!client.connected()) {
    connect();
  }

  client.loop();
   
  temperaturaUmidade();
  //Narcoleptic.delay(2000); // During this time power consumption is minimised

}

/* Conexão */
void connect() {
  Serial.print("Conectando Central...");
  while (!client.connect("Arduino_ImoTech", "d79d0bea", "2590fecb227150a0")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconectado!");

  if (!client.connect("Arduino_ImoTech")) {  

    /* Metodos Pub. e Sub. */
    publications();
    subscribies();

    /* Metodos de componentes */

    /* Estados inciais */
    digitalWrite(actionOff_Portao, HIGH);
    digitalWrite(actionOff_Porta, HIGH);
  }
}

/* Publicações */
void publications() {
    //client.publish("/myHouse/areaExterna/portao", "abrir");
    //Serial.println("Publicado");
    Serial.println("Publicaoes enviadas");
  }
  
/* Inscrições */
void subscribies() {
    client.subscribe("/myHouse/areaExterna/portao");
    client.subscribe("/myHouse/salaEstar/porta");
    
    client.subscribe("/myHouse/cozinha/janela");
    client.subscribe("/myHouse/quartoSuite/janela");
    client.subscribe("/myHouse/escritorio/janela");
    
    client.subscribe("/myHouse/geral/janelas");
    client.subscribe("/myHouse/geral/portas");
    
    Serial.println("Inscricoes atualizadas");
  }
  
/* Desinscrições */
void unsubscribies(){
    client.unsubscribe("/myHouse/portao");
  }

/* Temperatura e umidade */
void temperaturaUmidade() {
  Serial.println();
  Serial.println("Type,\tstatus,\tHumidity (%),\tTemperature (C)");
  Serial.print("DHT11, \t");
  int chk = DHT.read11(DHT11_PIN);
  
  switch (chk)
  {
    case DHTLIB_OK:  
    Serial.print("OK,\t"); 
    break;
    case DHTLIB_ERROR_CHECKSUM: 
    Serial.print("Checksum error,\t"); 
    break;
    case DHTLIB_ERROR_TIMEOUT: 
    Serial.print("Time out error,\t"); 
    break;
    default: 
    Serial.print("Unknown error,\t"); 
    break;
  }
    float t = DHT.temperature;
    double h = DHT.humidity;

//  // Check if any reads failed and exit early (to try again).
//  if (isnan(h) || isnan(t)) {
//    Serial.println("Failed to read from DHT sensor!");
//    return;
//  }

  char buffer[25];
  dtostrf(t,0, 0, buffer);
  client.publish("/myHouse/salaEstar/temperatura",buffer);
  //Serial.println(buffer);
  dtostrf(h,0, 0, buffer);
  client.publish("/myHouse/salaEstar/umidade",buffer);

//Serial.println(h);   
//Serial.print(",\t");
//Serial.print(t);
  
delay(5000);
}

void messageReceived(String topic, String payload, char * bytes, unsigned int length) {
  /* Area Externa - Portal */
  if(topic == "/myHouse/areaExterna/portao") {
    if(payload == "abrir"){
      digitalWrite(actionOn_Portao, HIGH);
      digitalWrite(actionOff_Portao, LOW);
      Serial.println("Portao Aberto");
    }else{
      digitalWrite(actionOn_Portao, LOW);
      digitalWrite(actionOff_Portao, HIGH);
      Serial.println("Portao Fechado");
    }
  } else {
    /* Sala de Estar */
    if(topic == "/myHouse/salaEstar/porta") {
      if(payload == "abrir"){
        digitalWrite(actionOn_Porta, HIGH);
        digitalWrite(actionOff_Porta, LOW);
        Serial.println("Porta [Sala Estar] Aberta");
      }else{
        digitalWrite(actionOn_Porta, LOW);
        digitalWrite(actionOff_Porta, HIGH);
        Serial.println("Porta [Sala Estar] Fechada");
      }
    } else {
      /* Cozinha */
      if(topic == "/myHouse/cozinha/janela") {
        if(payload == "abrir"){
          digitalWrite(openJanela_Cozinha, HIGH);
          Serial.println("Janela [Cozinha] Aberta");
        }else{
          digitalWrite(openJanela_Cozinha, LOW);
          Serial.println("Janela [Cozinha] Fechada");
        }
      } else {
        /* Quarto - Suite */
        if(topic == "/myHouse/quartoSuite/janela") {
          if(payload == "abrir"){
            digitalWrite(openJanela_QuartoSuite, HIGH);
            Serial.println("Janela [Quarto Suite] Aberta");
          }else{
            digitalWrite(openJanela_QuartoSuite, LOW);
            Serial.println("Janela [Quarto Suite] Fechada");
          }
        } else {
          /* Escritorio */
          if(topic == "/myHouse/escritorio/janela") {
            if(payload == "abrir"){
              digitalWrite(openJanela_Escritorio, HIGH);
              Serial.println("Janela [Escritorio] Aberta");
            }else{
              digitalWrite(openJanela_Escritorio, LOW);
              Serial.println("Janela [Escritorio] Fechada");
            }
          } else {
          /* Geral - Janelas */
          if(topic == "/myHouse/geral/janelas") {
            if(payload == "abrir"){
              digitalWrite(openJanela_Cozinha, HIGH);
              digitalWrite(openJanela_QuartoSuite, HIGH);
              digitalWrite(openJanela_Escritorio, HIGH);
              Serial.println("Todas janelas forao Aberta");
            }else{
              digitalWrite(openJanela_Cozinha, LOW);
              digitalWrite(openJanela_QuartoSuite, LOW);
              digitalWrite(openJanela_Escritorio, LOW);
              Serial.println("Todas janelas forao Fechadas");
              }
            } else {
            /* Geral - Portas */
            if(topic == "/myHouse/geral/portas") {
              if(payload == "abrir"){
                digitalWrite(actionOn_Portao, HIGH);
                digitalWrite(actionOff_Portao, LOW);
                digitalWrite(actionOn_Porta, HIGH);
                digitalWrite(actionOff_Porta, LOW);
                Serial.println("Todas janelas forao Aberta");
              }else{
                digitalWrite(actionOn_Portao, LOW);
                digitalWrite(actionOff_Portao, HIGH);
                digitalWrite(actionOn_Porta, LOW);
                digitalWrite(actionOff_Porta, HIGH);
                Serial.println("Todas janelas forao Fechadas");
                }
              }
            }
          }
        }
      } 
    } 
  } 
}
