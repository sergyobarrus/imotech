package com.example.sergiobarros.imotech;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

public class HumidityActivity extends AppCompatActivity {

    private static final String TAG = "TemperatureActivity";
    String vlrUmd = "0";
    TextView tvwHum;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If your minSdkVersion is 11 or higher use:
//        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_temperature);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Temperatura");
        setSupportActionBar(toolbar);

        tvwHum = (TextView) findViewById(R.id.tvwHum);

        sharedPref = getApplicationContext().getSharedPreferences("humidArd", Context.MODE_PRIVATE);

        new ClientSubscribe().ClientSubscribe("/myHouse/salaEstar/umidade", 1, getApplicationContext());

        new Thread(new Runnable() {
            public void run() {
                while (1 == 1) {
                    Log.d("CHECK", "Checando...");

                    String defaultValue = sharedPref.getString("umd", "0");
                    if (!defaultValue.equals(vlrUmd)) {
                        vlrUmd = defaultValue;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "Logout() >>> OK");
                                tvwHum.setText(vlrUmd + "%");
                            }
                        });
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}
