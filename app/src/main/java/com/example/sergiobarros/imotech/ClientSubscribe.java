package com.example.sergiobarros.imotech;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class ClientSubscribe implements MqttCallback {

    private static String PREF = "TemperatureActivity";
    private static final String TAG = "ClientSubscribe";
    MqttClient client;
    MqttConnectOptions connOpts;
    String broker = "tcp://broker.shiftr.io:1883";
    String clientId = "Android_ImoTech";
    String username = "d79d0bea";
    String userPass = "2590fecb227150a0";
    MemoryPersistence persistence = new MemoryPersistence();
    String topic = "";
    int qos = 0;
    Context context;
    SharedPreferences sharedPref;

    public ClientSubscribe() {}

    public void ClientSubscribe(String topic, int qos, Context context) {

        this.context = context;
        if (topic == "/myHouse/salaEstar/umidade") {
            sharedPref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
            Log.d(TAG, "prefUmid OK");
        } else {
            if (topic == "/myHouse/salaEstar/temperatura") {
                sharedPref = context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
                Log.d(TAG, "prefTemp OK");
            }
        }
        this.topic = topic;
        this.qos = qos;
        try {
            client = new MqttClient(broker, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setUserName(username);
            connOpts.setPassword(userPass.toCharArray());
            connOpts.setCleanSession(true);

            client.connect(connOpts);
            Log.d(TAG, "Conectou");

            client.setCallback(this);
            Log.d(TAG, "Call back");

            client.unsubscribe(topic);
            client.subscribe(topic, qos);
            Log.d(TAG, "Subscreveu");

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable cause) {
        if(!client.isConnected()) {
            try {
                Log.d(TAG, "Conexao perdida.");
                MqttConnectOptions connOpts = new MqttConnectOptions();
                connOpts.setUserName(username);
                connOpts.setPassword(userPass.toCharArray());
                connOpts.setCleanSession(true);

                client.connect(connOpts);
                client.setCallback(this);
                client.subscribe(topic, qos);
                Log.d(TAG, "Recuperou a conexao");

            } catch (MqttSecurityException e) {
                Log.d(TAG, "Conexao perida: erro 1");
                e.printStackTrace();

            } catch (MqttException e) {
                Log.d(TAG, "Conexao perida: erro 2");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.d(TAG, topic.toString());
        Log.d(TAG, message.toString());
        if (topic == "/myHouse/salaEstar/umidade") {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("auxUmidade", message.toString());
            editor.commit();
            Log.d(TAG, "SharedPreferences Comitado");
        } else {
            if (topic == "/myHouse/salaEstar/temperatura") {
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("auxTemperatura", message.toString());
                Log.d(TAG, "auxUmidade OK");
                editor.commit();
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        Log.d(TAG, "Mensagem completa");
    }
}