package com.example.sergiobarros.imotech;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String TAG = "MainActivity";
    SharedPreferences sharedPref;

    private Button mBtn_casaGeral, mBtn_areaExterna, mBtn_salaEstar, mBtn_cozinha, mBtn_quartoSuite, mBtn_escritorio;
    private Button mBtn_portaoAbrir, mBtn_portaoFechar,mBtn_geralPortasAbrir, mBtn_geralPortasFechar, mBtn_salaEstar_portaAbrir,
            mBtn_salaEstar_portaFechar,mBtn_cozinhaJanelaAbrir, mBtn_cozinhaJanelaFechar, mBtn_quartoSuiteJanelaAbrir, mBtn_quartoSuiteJanelaFechar,
            mBtn_geralJanelasAbrir, mBtn_geralJanelasFechar, mBtn_escritorioJanelaAbrir, mBtn_escritorioJanelaFechar;
    TextView txt_emailUser, txt_status;
    private ExpandableRelativeLayout mExpandLayout;


    // declare_auth
    private FirebaseAuth mAuth;

    // declare_auth_listener
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_status = (TextView) findViewById(R.id.txt_infoStatus);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Drawer Layout
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Navigation View
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        sharedPref = getApplicationContext().getSharedPreferences("humidArd", Context.MODE_PRIVATE);

        // initialize_auth]
        mAuth = FirebaseAuth.getInstance();

        // auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    txt_emailUser = (TextView) findViewById(R.id.txt_emailUser);
                    txt_emailUser.setText(user.getEmail());
                } else {
                    // User is signed out
                    Log.d(TAG, "Usuario desconectado!");
                }
            }
        };


        // Conexao Geral
        mBtn_casaGeral = (Button) findViewById(R.id.btn_casaGeral);
        mBtn_casaGeral.setOnClickListener(this);

            mBtn_geralPortasAbrir = (Button) findViewById(R.id.btn_geralPortasAbrir);
            mBtn_geralPortasAbrir.setOnClickListener(this);

            mBtn_geralPortasFechar = (Button) findViewById(R.id.btn_geralPortasFechar);
            mBtn_geralPortasFechar.setOnClickListener(this);

            mBtn_geralJanelasAbrir = (Button) findViewById(R.id.btn_geralJanelasAbrir);
            mBtn_geralJanelasAbrir.setOnClickListener(this);

            mBtn_geralJanelasFechar = (Button) findViewById(R.id.btn_geralJanelasFechar);
            mBtn_geralJanelasFechar.setOnClickListener(this);

        // ---------------------------------------------------------------

        mBtn_areaExterna = (Button) findViewById(R.id.btn_areaExternas);
        mBtn_areaExterna.setOnClickListener(this);

            mBtn_portaoAbrir = (Button) findViewById(R.id.btn_portaoAbrir);
            mBtn_portaoAbrir.setOnClickListener(this);

            mBtn_portaoFechar = (Button) findViewById(R.id.btn_portaoFechar);
            mBtn_portaoFechar.setOnClickListener(this);

        // ---------------------------------------------------------------

        mBtn_salaEstar = (Button) findViewById(R.id.btn_salaEstar);
        mBtn_salaEstar.setOnClickListener(this);

            mBtn_salaEstar_portaAbrir = (Button) findViewById(R.id.btn_salaEstar_portaAbrir);
            mBtn_salaEstar_portaAbrir.setOnClickListener(this);

            mBtn_salaEstar_portaFechar = (Button) findViewById(R.id.btn_salaEstar_portaFechar);
            mBtn_salaEstar_portaFechar.setOnClickListener(this);

        // ---------------------------------------------------------------

        mBtn_cozinha = (Button) findViewById(R.id.btn_cozinha);
        mBtn_cozinha.setOnClickListener(this);

            mBtn_cozinhaJanelaAbrir = (Button) findViewById(R.id.btn_cozinhaJanelaAbrir);
            mBtn_cozinhaJanelaAbrir.setOnClickListener(this);

            mBtn_cozinhaJanelaFechar = (Button) findViewById(R.id.btn_cozinhaJanelaFechar);
            mBtn_cozinhaJanelaFechar.setOnClickListener(this);

        // ---------------------------------------------------------------

        mBtn_quartoSuite = (Button) findViewById(R.id.btn_quartoSuite);
        mBtn_quartoSuite.setOnClickListener(this);

            mBtn_quartoSuiteJanelaAbrir= (Button) findViewById(R.id.btn_quartoSuiteJanelaAbrir);
            mBtn_quartoSuiteJanelaAbrir.setOnClickListener(this);

            mBtn_quartoSuiteJanelaFechar = (Button) findViewById(R.id.btn_quartoSuiteJanelaFechar);
            mBtn_quartoSuiteJanelaFechar.setOnClickListener(this);

        // ---------------------------------------------------------------

        mBtn_escritorio = (Button) findViewById(R.id.btn_escritorio);
        mBtn_escritorio.setOnClickListener(this);

            mBtn_escritorioJanelaAbrir = (Button) findViewById(R.id.btn_escritorioJanelaAbrir);
            mBtn_escritorioJanelaAbrir.setOnClickListener(this);

            mBtn_escritorioJanelaFechar = (Button) findViewById(R.id.btn_escritorioJanelaFechar);
            mBtn_escritorioJanelaFechar.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        ExpandableRelativeLayout expAreaExterna, expSalaEstar, expcasaGeral, expCozinha, expQuartoSuite, expEscritorio;
        expcasaGeral = (ExpandableRelativeLayout) findViewById(R.id.exp_casaGeral);
        expAreaExterna = (ExpandableRelativeLayout) findViewById(R.id.exp_areaExterna);
        expSalaEstar = (ExpandableRelativeLayout) findViewById(R.id.exp_salaEstar);
        expCozinha = (ExpandableRelativeLayout) findViewById(R.id.exp_cozinha);
        expQuartoSuite = (ExpandableRelativeLayout) findViewById(R.id.exp_quartoSuite);
        expEscritorio = (ExpandableRelativeLayout) findViewById(R.id.exp_escritorio);

    switch (v.getId()) {

        case R.id.btn_casaGeral:
            expcasaGeral.toggle(); // toggle expand, collapse
            break;

            case R.id.btn_geralPortasAbrir:
                new ClientPublish().ClientPublish("/myHouse/geral/portas", "abrir", 0);
                txt_status.setText("Todas as Portas forao Abertas!");
                break;

            case R.id.btn_geralPortasFechar:
                new ClientPublish().ClientPublish("/myHouse/geral/portas", "fechar", 0);
                txt_status.setText("Todas as Portas forao Fechadas!");
                break;

            case R.id.btn_geralJanelasAbrir:
                new ClientPublish().ClientPublish("/myHouse/geral/janelas", "abrir", 0);
                txt_status.setText("Todas as Janelas forao Abertas!");
                break;

            case R.id.btn_geralJanelasFechar:
                new ClientPublish().ClientPublish("/myHouse/geral/janelas", "fechar", 0);
                txt_status.setText("Todas as Janelas forao Fechadas!");
                break;

        //--------------------------------------------------------------------------

        case R.id.btn_areaExternas:
            expAreaExterna.toggle();
            break;

            case R.id.btn_portaoAbrir:
                new ClientPublish().ClientPublish("/myHouse/areaExterna/portao", "abrir", 0);
                txt_status.setText("O Portao de entrada foi Aberto!");
                break;

            case R.id.btn_portaoFechar:
                new ClientPublish().ClientPublish("/myHouse/areaExterna/portao", "fechar", 0);
                txt_status.setText("O Portao de entrada foi Fechado!");
                break;

        //--------------------------------------------------------------------------

        case R.id.btn_salaEstar:
            expSalaEstar.toggle();
            break;

            case R.id.btn_salaEstar_portaAbrir:
                new ClientPublish().ClientPublish("/myHouse/salaEstar/porta", "abrir", 0);
                txt_status.setText("A Porta de entrada externa foi Aberta!");
                break;

            case R.id.btn_salaEstar_portaFechar:
                new ClientPublish().ClientPublish("/myHouse/salaEstar/porta", "fechar", 0);
                txt_status.setText("A Porta de entrada externa foi Aberta!");
                break;

        //--------------------------------------------------------------------------

        case R.id.btn_cozinha:
            expCozinha.toggle();
            break;

            case R.id.btn_cozinhaJanelaAbrir:
                new ClientPublish().ClientPublish("/myHouse/cozinha/janela", "abrir", 0);
                break;

            case R.id.btn_cozinhaJanelaFechar:
                new ClientPublish().ClientPublish("/myHouse/cozinha/janela", "fechar", 0);
                break;

        //--------------------------------------------------------------------------

        case R.id.btn_quartoSuite:
            expQuartoSuite.toggle();
            break;

            case R.id.btn_quartoSuiteJanelaAbrir:
                new ClientPublish().ClientPublish("/myHouse/quartoSuite/janela", "abrir", 0);
                break;

            case R.id.btn_quartoSuiteJanelaFechar:
                new ClientPublish().ClientPublish("/myHouse/quartoSuite/janela", "fechar", 0);
                break;

        //--------------------------------------------------------------------------

        case R.id.btn_escritorio:
            expEscritorio.toggle();
            break;

            case R.id.btn_escritorioJanelaAbrir:
                new ClientPublish().ClientPublish("/myHouse/escritorio/janela", "abrir", 0);
                break;

            case R.id.btn_escritorioJanelaFechar:
                new ClientPublish().ClientPublish("/myHouse/escritorio/janela", "fechar", 0);
                break;

        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_controle) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_temperatura) {
            Intent intent = new Intent(getApplicationContext(), TemperatureActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_umidade) {
            Intent intent = new Intent(getApplicationContext(), HumidityActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            mAuth.signOut();

            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.user_sign_out, Snackbar.LENGTH_LONG).setAction("Action", null);
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(getResources().getColor(R.color.colorGray));
            snackbar.show();

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);

            Log.d(TAG, "Logout() >>> OK");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
}