/*
* https://shiftr.io/
* http://www.eclipse.org/paho/clients/java/
* http://www.eclipse.org/paho/files/javadoc/index.html
* */

package com.example.sergiobarros.imotech;

import android.util.Log;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class ClientPublish implements MqttCallback {

    private static final String TAG = "ClientPublish";
    MqttClient client;
    MqttConnectOptions connOpts;
    String broker = "tcp://broker.shiftr.io:1883";
    String clientId = "Android_ImoTech";
    String username = "d79d0bea";
    String userPass = "2590fecb227150a0";
    MemoryPersistence persistence = new MemoryPersistence();
    String topic = "";
    int qos = 0;
    
    public ClientPublish() {}

    public boolean ClientPublish(String topic, String message, int qos ) {
        this.topic = topic;
        this.qos = qos;
        try {
            client = new MqttClient(broker, clientId, persistence);
            if(!client.isConnected()) {
                connOpts = new MqttConnectOptions();
                connOpts.setUserName(username);
                connOpts.setPassword(userPass.toCharArray());
                connOpts.setCleanSession(true);
                Log.d(TAG, "Conectando ao broker: " + broker);

                client.connect(connOpts);
                Log.d(TAG, "Conectado");

                MqttMessage messagem = new MqttMessage(message.getBytes());
                messagem.setQos(qos);
                client.publish(topic, messagem);
                Log.d(TAG, "Conteudo: " +topic +" | "+ messagem);
                Log.d(TAG, "Mensagem envidada");

                client.disconnect();
                Log.d(TAG, "Desconectado.");
                return true;
            }
        } catch(MqttException me) {
            me.printStackTrace();
        }
        return false;
    }

    @Override
    public void connectionLost(Throwable cause) {
        if(!client.isConnected()) {
            try {
                Log.d(TAG, "connectionLost >>> Conexao perdida.");
                connOpts = new MqttConnectOptions();
                connOpts.setUserName(username);
                connOpts.setPassword(userPass.toCharArray());
                connOpts.setCleanSession(true);

                client.connect(connOpts);
                client.setCallback(this);
                client.subscribe(topic, qos);

                Log.d(TAG, "connectionLost >>> Recuperou a conexao");
            } catch (MqttSecurityException e) {
                Log.d(TAG, "connectionLost >>> Conexao perida: erro 1");
                e.printStackTrace();
            } catch (MqttException e) {
                Log.d(TAG, "connectionLost >>> Conexao perida: erro 2");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.d(TAG, "messageArrived >>> " +message.toString());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        Log.d(TAG, "deliveryComplete >>> Mensagem completa");
    }
}

