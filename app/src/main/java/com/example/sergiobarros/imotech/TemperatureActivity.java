package com.example.sergiobarros.imotech;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

public class TemperatureActivity extends AppCompatActivity {

    private static String PREF = "TemperatureActivity";
    private static final String TAG = "TemperatureActivity";
    String vlrTemp = "0";
    TextView tvwTemp;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If your minSdkVersion is 11 or higher use:
        setContentView(R.layout.activity_temperature);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Temperatura");
        setSupportActionBar(toolbar);

        tvwTemp = (TextView) findViewById(R.id.tvwTemp);

        //shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);

       // sharedPref = getApplicationContext().getSharedPreferences( "prefTemp", Context.MODE_PRIVATE);
       sharedPref = getSharedPreferences( PREF, Context.MODE_PRIVATE);

        new ClientSubscribe().ClientSubscribe("/myHouse/salaEstar/temperatura", 1, getApplicationContext());

        new Thread(new Runnable() {
            public void run() {
                while (1 == 1) {
                    Log.d("CHECK", "Checando...");
                    String defaultValue = sharedPref.getString("auxTemperatura", "0");
                    if (!defaultValue.equals(vlrTemp)) {
                        vlrTemp = defaultValue;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.d(TAG, "Valor inserido"+vlrTemp);
                                tvwTemp.setText(vlrTemp.toString() + "C");
                            }
                        });
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}
