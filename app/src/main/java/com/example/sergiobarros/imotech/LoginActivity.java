package com.example.sergiobarros.imotech;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.ArrayList;
import java.util.List;

import authentication.BaseActivity;
import authentication.EmailPasswordActivity;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private static final int RC_SIGN_IN = 9001;

    // UI references.
    private GoogleApiClient mGoogleApiClient;
    private AutoCompleteTextView mEmailTxt;
    private EditText mPasswordTxt;

    // declare_auth
    private FirebaseAuth mAuth;

    // declare_auth_listener
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Facebook Initialize
        setContentView(R.layout.activity_login);

        // Edit Views
        mEmailTxt = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordTxt = (EditText) findViewById(R.id.password);

        // Buttons
        findViewById(R.id.signin_emailpassword).setOnClickListener(this);
        findViewById(R.id.create_emailpassword).setOnClickListener(this);


        // initialize_auth]
        mAuth = FirebaseAuth.getInstance();

        // auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged: signed_in: " + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged: signed_out ");
                }
                // [START_EXCLUDE]
                updateUI(user);
            }
        };
        Log.d(TAG, "onCreate() >>> OK");
    }

    // EMAIL PASSWORD ACCOUNT ------------------------------------------------------------------------

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + mEmailTxt.getText().toString());
        if (!validateForm()) {
            return;
        }
        showProgressDialog();

        // create_user_with_email
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (task.isSuccessful()) {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.auth_success, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null);
                            View sbView = snackbar.getView();
                            sbView.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                            snackbar.show();
                        } else {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.auth_failed, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null);
                            View sbView = snackbar.getView();
                            sbView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                            snackbar.show();
                        }
                        hideProgressDialog();
                    }
                });
        Log.d(TAG, "createAccount() >>> OK");
    }

    public void signInEmailPassword(String email, String password) {
        Log.d(TAG, "signInEmailPassword: " + email);
        if (!validateForm()) {
            return;
        }
        showProgressDialog();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());

                            // Notification success
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.auth_failed, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null);
                            View sbView = snackbar.getView();
                            sbView.setBackgroundColor(getResources().getColor(R.color.colorRed));
                            snackbar.show();
                        }

                        if (!task.isSuccessful()) {
//                      mStatusTextView.setText(R.string.auth_failed);
                        }
                        hideProgressDialog();
                    }
                });
        Log.d(TAG, "signInEmailPassword() >>> OK");
    }

    public void signOutEmailPassword() {
        mAuth.signOut();
        updateUI(null);
        Log.d(TAG, "signOutEmailPassword() >>> OK");
    }

    // ALL ACCOUNT ------------------------------------------------------------------------

    // On start add listener
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    // On stop remove listener
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();

        if (i == R.id.signin_emailpassword) {
            signInEmailPassword(mEmailTxt.getText().toString(), mPasswordTxt.getText().toString());

        } else if (i == R.id.create_emailpassword) {
            createAccount(mEmailTxt.getText().toString(), mPasswordTxt.getText().toString());
        }
        Log.d(TAG, "onClick() >>> OK");
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailTxt.getText().toString();
        String password = mPasswordTxt.getText().toString();

        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.email_senha_empty, Snackbar.LENGTH_LONG).setAction("Action", null);
            snackbar.show();
            valid = false;

        }else if (TextUtils.isEmpty(email)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.email_empty, Snackbar.LENGTH_LONG).setAction("Action", null);
            snackbar.show();
            valid = false;

        } else if (TextUtils.isEmpty(password)) {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.senha_empty, Snackbar.LENGTH_LONG).setAction("Action", null);
            snackbar.show();
            valid = false;

        } else {
            mPasswordTxt.setError(null);
        }
        Log.d(TAG, "validateForm() >>> OK");
        return valid;
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        } else {
            resetFields();
            findViewById(R.id.create_emailpassword).setVisibility(View.VISIBLE);
            findViewById(R.id.signin_emailpassword).setVisibility(View.VISIBLE);
        }
        Log.d(TAG, "updateUI() >>> OK");
    }

    private void resetFields() {
        mEmailTxt.setText(null);
        mPasswordTxt.setText(null);
        mEmailTxt.requestFocus();
        Log.d(TAG, "resetFields() >>> OK");
    }
}

